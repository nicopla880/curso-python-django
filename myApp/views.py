from django.shortcuts import render
from .forms import InventarioForm
from .models import Inventario

# Create your views here.
def hola(request):
    user_name = request.GET.get('name', '')
    form = InventarioForm()
    context = {}
    context['nombre'] = user_name
    context['form'] = form
    if request.method == 'POST':
        form = InventarioForm(data=request.POST)
        if form.is_valid():
            form.save()
    inventario_list = Inventario.objects.all()
    context['lista'] = inventario_list
    return render(request,
                 'base.html',
                  context,)